FROM gradle:alpine
USER root

ADD . .
RUN [ "gradle", "build", "--stacktrace" ]
COPY gradle.properties /home/gradle/.gradle/gradle.properties
VOLUME /tmp
ENV JAVA_OPTS=""

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar -DDATABASE_USERNAME=sa -DDATABASE_PASSWORD=Passw0rd -DDATABASE_SERVER=localhost -DDATABASE_PORT=1433 -DDATABASE_NAME=KALDISDB -DRLS_DATABASE_PORT=1433 -DRLS_DATABASE_NAME=KTALDB -DRLS_DATABASE_USERNAME=sa -DRLS_DATABASE_PASSWORD=Passw0rd -DRLS_DATABASE_SERVER=localhost /home/gradle/build/libs/kaldis-etl-0.0.1-SNAPSHOT.jar" ]

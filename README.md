# Kotlin API Boilerplate


## Build
```sh
$ gradle clean build
```

## Run from gradle project
```sh
$ gradle clean bootRun
```
- api going to available on http://localhost:8080/api/v1
- with test api going to available on path http://localhost:8080/api/v1/students/0002

## Run from artifact
```sh
$ java -jar build/libs/api-0.0.1-SNAPSHOT.jar
```

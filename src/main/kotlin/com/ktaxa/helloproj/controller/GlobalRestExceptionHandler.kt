package com.ktaxa.helloproj.controller

import com.ktaxa.helloproj.controller.expection.DataNotFoundException
import com.ktaxa.helloproj.dto.ErrorResponseDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*


@ControllerAdvice
class GlobalRestExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Exception::class)
    fun handleAllExceptions(ex: Exception, request: WebRequest?): ResponseEntity<Any?> {
        val details: MutableList<String> = ArrayList()
        ex.message?.let { details.add(ex.message!!) }

        val error = ErrorResponseDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Server Error", details)
        return ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(DataNotFoundException::class)
    fun handleDataNotFoundException(ex: DataNotFoundException, request: WebRequest?): ResponseEntity<Any?> {
        val details: MutableList<String> = ArrayList()
        ex.message?.let {  details.add(ex.message) }
        val error = ErrorResponseDTO(HttpStatus.NOT_FOUND.value(), "Data not found", details)
        return ResponseEntity(error, HttpStatus.NOT_FOUND)
    }
}

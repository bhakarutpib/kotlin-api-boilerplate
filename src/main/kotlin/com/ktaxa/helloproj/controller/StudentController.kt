package com.ktaxa.helloproj.controller

import com.ktaxa.helloproj.controller.expection.DataNotFoundException
import com.ktaxa.helloproj.dto.StudentDTO
import com.ktaxa.helloproj.service.StudentService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("students")
@Api(description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of Student.")
class StudentController(private val studentService: StudentService) {

    @GetMapping("/{id}")
    @ApiOperation(value = "student",
            notes = "get student information by id",
            response = StudentDTO::class,
            responseContainer = "Object"
    )
    fun getStudent(@PathVariable id: String): ResponseEntity<StudentDTO>? {
        val student = studentService.findOne(id)

        student ?: throw DataNotFoundException("no student found")

        return ResponseEntity.ok(student)
    }
}

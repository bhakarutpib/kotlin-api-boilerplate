package com.ktaxa.helloproj.constant

class EnvConst {
    companion object {
        // mandatory
        @JvmStatic val MDC_TRANSACTION_ID_KEY:String = "transaction.id"
        @JvmStatic val MDC_TRANSACTION_OWNER_KEY:String = "transaction.owner"
        @JvmStatic val API_KEY:String = "API_KEY"
        @JvmStatic val API_SECRET:String = "API_SECRET"
        @JvmStatic val CONSUMER_USERNAME:String = "CONSUMER_USERNAME"
        @JvmStatic val PROXY_HOST:String = ""
        @JvmStatic val PROXY_PORT:Int = -1

        @JvmStatic val HTTP_HEADER_CORRELATION_KEY:String = "correlation"
        @JvmStatic val HTTP_HEADER_API_KEY:String = "apikey"
        @JvmStatic val HTTP_HEADER_API_SECRET:String = "apisecret"
        @JvmStatic val HTTP_HEADER_API_CONSUMER:String = "x-consumer-username"

        // uncomment a line below when need to authentication with JWKS
//        @JvmStatic val JWKS:String = System.getenv("JWKS").trim()

        @JvmStatic val AUTHORIZATION_HEADER:String = "Authorization"
    }
}

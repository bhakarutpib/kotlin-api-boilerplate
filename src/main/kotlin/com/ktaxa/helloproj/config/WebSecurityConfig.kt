package com.ktaxa.helloproj.config

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.context.SecurityContextHolder

@Configuration
@EnableWebSecurity(debug = true)
class WebSecurityConfig(
        // uncomment a line below when need to authentication with JWKS
//        private val processor: ConfigurableJWTProcessor<SecurityContext>
) : WebSecurityConfigurerAdapter() {
    private val logger = LoggerFactory.getLogger(WebSecurityConfig::class.java)

    init {
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL)
    }

    override fun configure(http: HttpSecurity) {
        logger.debug("WebSecurityConfig initial data")
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                // handle secure path wih a line below
                .antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/swagger-ui.html**", "/webjars/**", "/students/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // uncomment a line below when need to authentication with JWKS
//        http.addFilterAfter(AuthFilter(processor, authenticationManager()), UsernamePasswordAuthenticationFilter::class.java)
    }
}

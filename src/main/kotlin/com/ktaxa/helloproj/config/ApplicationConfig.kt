package com.ktaxa.helloproj.config

import com.ktaxa.helloproj.config.resttemplate.CustomRestTemplateCustomizer
import org.apache.http.HttpResponse
import org.apache.http.conn.ConnectionKeepAliveStrategy
import org.apache.http.message.BasicHeaderElementIterator
import org.apache.http.protocol.HTTP
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.DependsOn
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import java.util.*

@Configuration
@ComponentScan("com.ktaxa.helloproj.config")
class ApplicationConfig {
    // uncomment a line below when need to authentication with JWKS
//    @Bean
//    fun configurableJWTProcessor(): ConfigurableJWTProcessor<SecurityContext> {
//        logger.info("EnvConst.JWKS:::: {}", EnvConst.JWKS)
//        val jwkSet = JWKSet.parse(EnvConst.JWKS)
//        logger.info("--configurableJWTProcessor--")
//        logger.info("[jwkSet : $jwkSet]")
//        val keySource: JWKSource<SecurityContext> = ImmutableJWKSet(jwkSet)
//        val jwtProcessor: ConfigurableJWTProcessor<SecurityContext> = DefaultJWTProcessor()
//        val keySelector = JWSVerificationKeySelector(JWSAlgorithm.RS256, keySource)
//        jwtProcessor.jwsKeySelector = keySelector
//        logger.info("-JwtProcessor: Selector JWKS-")
//        return jwtProcessor
//    }

    @Bean
    fun connectionKeepAliveStrategy(): ConnectionKeepAliveStrategy {
        return object : ConnectionKeepAliveStrategy {
            override fun getKeepAliveDuration(response: HttpResponse?, context: org.apache.http.protocol.HttpContext?): Long {
                val it = BasicHeaderElementIterator(response?.headerIterator(HTTP.CONN_KEEP_ALIVE))
                while (it.hasNext()) {
                    val he = it.nextElement()
                    val param = he.name
                    val value = he.value

                    if (value != null && param.equals("timeout", ignoreCase = true)) {
                        return try {
                            java.lang.Long.parseLong(value) * 1000
                        } catch (ex: NumberFormatException) {
                            1000
                        }
                    }
                }
                return 30 * 1000
            }
        }
    }

    @Bean
    @Qualifier("customRestTemplateCustomizer")
    fun customRestTemplateCustomizer(): CustomRestTemplateCustomizer {
        return CustomRestTemplateCustomizer()
    }


    @Bean
    @DependsOn(value = ["customRestTemplateCustomizer"])
    fun restTemplateBuilder(): RestTemplateBuilder {
        return RestTemplateBuilder(customRestTemplateCustomizer())
    }

    @Bean
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate {
        val messageConverters = ArrayList<HttpMessageConverter<*>>()
        val converter = MappingJackson2HttpMessageConverter()
        converter.supportedMediaTypes = Collections.singletonList(MediaType.ALL)
        messageConverters.add(converter)

        val timeout = 30 * 1000 // 30 sec
        return builder
                .messageConverters(messageConverters)
                .setReadTimeout(timeout)
                .setConnectTimeout(timeout)
                .build()
    }
}

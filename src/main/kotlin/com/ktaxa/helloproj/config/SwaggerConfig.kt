package com.ktaxa.helloproj.config

import com.ktaxa.helloproj.constant.EnvConst
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig {
    companion object {
        const val DEFAULT_INCLUDE_PATTERN = "/(customers|documents|payments|policies)/.*"
    }

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                // Sets up the security schemes used to protect the apis.
                // Supported schemes are ApiKey, BasicAuth and OAuth
                .securitySchemes(listOf(apiKey()))
                // Provides a way to globally set up security contexts for operation.
                // The idea here is that we provide a way to select operations to be protected
                // by one of the specified security schemes.
                .securityContexts(listOf(securityContext()))

                // the default response messages are not added to the global response messages
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ktaxa.helloproj.controller"))
                .paths(PathSelectors.any())
                .build()

    }

    private fun getApiInfo(): ApiInfo {
        val contact = Contact("KTAXA", "", "")
        return ApiInfoBuilder()
                .title("KTAXA Api")
                .description("KTAXA Api Definition")
                .version("1.0.0")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
                .contact(contact)
                .build()
    }

    private fun apiKey(): ApiKey {
        // Here we use ApiKey as the security schema that is identified by the name JWT
        return ApiKey("JWT", EnvConst.AUTHORIZATION_HEADER, "header")
    }

    private fun securityContext(): SecurityContext {
        // Selector for the paths this security context applies to.
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
                .build()
    }

    fun defaultAuth(): List<SecurityReference> {
        val authorizationScope = AuthorizationScope("global", "accessEverything")
        val authorizationScopes = arrayOfNulls<AuthorizationScope>(1)
        authorizationScopes[0] = authorizationScope

        // Here we use the same key defined in the security scheme JWT
        return listOf(SecurityReference("JWT", authorizationScopes))
    }
}

package com.ktaxa.helloproj.config.mdc

import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.core.task.TaskDecorator

class MdcTaskDecorator : TaskDecorator {
    private val logger = LoggerFactory.getLogger(MdcTaskDecorator::class.java)

    override fun decorate(runnable: Runnable): Runnable {
        return try {
            val mdcContext = MDC.getCopyOfContextMap()
            Runnable {
                MDC.setContextMap(mdcContext)
                runnable.run()
            }
        } catch (e: Exception){
            logger.warn("decorate task exceptions: {}", e)
            runnable
        }
    }
}

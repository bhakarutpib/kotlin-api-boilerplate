package com.ktaxa.helloproj.config.resttemplate

import com.ktaxa.helloproj.constant.EnvConst
import org.jboss.logging.MDC
import org.slf4j.LoggerFactory
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.client.support.HttpRequestWrapper
import org.springframework.security.core.context.SecurityContextHolder
import java.io.IOException

class CustomClientHttpRequestInterceptor : ClientHttpRequestInterceptor {
    private val logger = LoggerFactory.getLogger(CustomClientHttpRequestInterceptor::class.java)

    @Throws(IOException::class)
    override fun intercept(request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution): ClientHttpResponse {
        setRequestHeaders(request)
        val wrapper = HttpRequestWrapper(request)
        val start = System.currentTimeMillis()
        val response = execution.execute(wrapper, body)
        val end = System.currentTimeMillis()
        val consumedTimeMillis = end - start
        val args = arrayOf(request.headers, request.method, request.uri, Thread.currentThread().name)

        logger.info(mapOf<String, Any?>(
                "method" to "intercept",
                "args" to args.joinToString(","),
                "return" to response.toString(),
                "message" to "rest template was executed in ${consumedTimeMillis / 1000F}"
        ).toString())

        return response
    }

    private fun setRequestHeaders(request: HttpRequest) {
        val credential = SecurityContextHolder.getContext().authentication.credentials

        if (credential.toString().isNotEmpty()) {
            request.headers.set(EnvConst.AUTHORIZATION_HEADER, "Bearer $credential")
        }

        request.headers.set(EnvConst.HTTP_HEADER_API_KEY, EnvConst.API_KEY)
        request.headers.set(EnvConst.HTTP_HEADER_API_SECRET, EnvConst.API_SECRET)
        request.headers.set(EnvConst.HTTP_HEADER_API_CONSUMER, EnvConst.CONSUMER_USERNAME)
        request.headers.set(EnvConst.HTTP_HEADER_CORRELATION_KEY, getCorrelationID())
    }

    private fun getCorrelationID(): String {
        return try {
            MDC.get(EnvConst.MDC_TRANSACTION_ID_KEY) as String
        } catch (e: Exception) {
            ""
        }
    }
}

package com.ktaxa.helloproj.config.resttemplate

import com.ktaxa.helloproj.constant.EnvConst
import org.apache.http.HttpHost
import org.apache.http.conn.ConnectionKeepAliveStrategy
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.client.HttpClients
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateCustomizer
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit

class CustomRestTemplateCustomizer : RestTemplateCustomizer {
    @Autowired
    private val connectionKeepAliveStrategy: ConnectionKeepAliveStrategy? = null

    companion object {
        private val LOGGER = LoggerFactory.getLogger(CustomRestTemplateCustomizer::class.java)
    }

    override fun customize(restTemplate: RestTemplate) {
        LOGGER.info("Customize REST Template")
        setRequestFactory(restTemplate)
        addInterceptors(restTemplate)
    }

    private fun setRequestFactory(restTemplate: RestTemplate) {
        val acceptingTrustStrategy = { chain: Array<X509Certificate>, authType: String -> true }
        val sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build()
        val csf = SSLConnectionSocketFactory(sslContext)

        val httpClient = if (EnvConst.PROXY_HOST.isNotEmpty() && EnvConst.PROXY_PORT != -1) {
            HttpClientBuilder
                    .create()
                    .setSSLSocketFactory(csf)
                    .setProxy(HttpHost(EnvConst.PROXY_HOST, EnvConst.PROXY_PORT, "http"))
                    .setMaxConnTotal(100)
                    .setMaxConnPerRoute(100)
                    .setKeepAliveStrategy(connectionKeepAliveStrategy)
                    .evictIdleConnections(30, TimeUnit.SECONDS)
                    .build()
        } else {
            HttpClients
                    .custom()
                    .setSSLSocketFactory(csf)
                    .setMaxConnTotal(100)
                    .setMaxConnPerRoute(100)
                    .setKeepAliveStrategy(connectionKeepAliveStrategy)
                    .evictIdleConnections(30, TimeUnit.SECONDS)
                    .build()
        }
        restTemplate.requestFactory = HttpComponentsClientHttpRequestFactory(httpClient)
    }

    private fun addInterceptors(restTemplate: RestTemplate) {
        restTemplate.interceptors.add(CustomClientHttpRequestInterceptor())
    }
}

package com.ktaxa.helloproj.config

import com.ktaxa.helloproj.config.mdc.MdcTaskDecorator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import java.util.concurrent.Executor


@Configuration
@EnableAsync
class AsyncConfig {

    @Bean("threadPoolTaskExecutor")
    fun asyncExecutor(): Executor {
        val executor = ThreadPoolTaskExecutor()
        executor.setWaitForTasksToCompleteOnShutdown(true)
        executor.setTaskDecorator(MdcTaskDecorator())
        executor.initialize()
        return executor
    }
}

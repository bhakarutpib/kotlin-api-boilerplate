package com.ktaxa.helloproj.dto

data class StudentDTO(
        val id: String,
        val name: String,
        val passportNumber: String
)

package com.ktaxa.helloproj.dto

data class ErrorResponseDTO(
        val code: Int,
        val message: String?,
        val details: MutableList<String>?
)

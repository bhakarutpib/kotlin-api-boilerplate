package com.ktaxa.helloproj.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class TokenClaimsDTO(
        @JsonProperty("auth_time")
        val authTime: Int = 0,

        @JsonProperty("client_id")
        val clientId: String = "",

        @JsonProperty("event_id")
        val eventId: String = "",

        @JsonProperty("token_use")
        val tokenUse: String = "",
        val exp: Int = 0,
        val iat: Int = 0,
        val iss: String = "",
        val jti: String = "",
        val scope: String = "",
        val sub: String = "",
        val username: String = "anonymous"
)

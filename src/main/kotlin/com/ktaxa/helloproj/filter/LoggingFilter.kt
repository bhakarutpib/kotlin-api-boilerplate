package com.ktaxa.helloproj.filter

import com.google.gson.Gson
import com.ktaxa.helloproj.constant.EnvConst
import com.ktaxa.helloproj.dto.TokenClaimsDTO
import com.nimbusds.jwt.JWTClaimsSet
import org.slf4j.MDC
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LoggingFilter : OncePerRequestFilter() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        try {
            val authentication = SecurityContextHolder.getContext().authentication
            if (authentication != null) {
                val principal = SecurityContextHolder.getContext().authentication.principal
                val user = getUserFromPrincipal(principal)
                val correlationId = getCorrelationID(user)
                MDC.put(EnvConst.MDC_TRANSACTION_OWNER_KEY, user)
                MDC.put(EnvConst.MDC_TRANSACTION_ID_KEY, correlationId)
            }
        } catch (e: Exception) {
            logger.debug(e.message)
        } finally {
            chain.doFilter(request, response)
            MDC.remove(EnvConst.MDC_TRANSACTION_OWNER_KEY)
            MDC.remove(EnvConst.MDC_TRANSACTION_ID_KEY)
        }

    }

    private fun getUserFromPrincipal(principal: Any): String {
        return try {
            if (principal is JWTClaimsSet) {
                val gson = Gson()
                val claims = gson.fromJson(principal.toString(), TokenClaimsDTO::class.java)
                claims.username
            } else {
                principal.toString()
            }

        } catch (e: Exception) {
            logger.debug(e.message)
            principal.toString()
        }
    }

    private fun getCorrelationID(user: String): String {
        return user + "-" + UUID.randomUUID()
    }
}

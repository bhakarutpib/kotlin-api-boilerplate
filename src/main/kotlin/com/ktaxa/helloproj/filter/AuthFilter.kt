package com.ktaxa.helloproj.filter

import com.ktaxa.helloproj.constant.EnvConst
import com.ktaxa.helloproj.dto.AuthenticationTokenDTO
import com.nimbusds.jose.proc.SecurityContext
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor
import org.slf4j.LoggerFactory
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthFilter(
        private val processor: ConfigurableJWTProcessor<SecurityContext>,
        authenticationManager: AuthenticationManager
) : BasicAuthenticationFilter(authenticationManager) {
    companion object {
        private val logger = LoggerFactory.getLogger(AuthFilter::class.java)
    }

    override fun doFilterInternal(
            req: HttpServletRequest,
            res: HttpServletResponse,
            chain: FilterChain
    ) {
        try {
            val token = extractToken(req.getHeader(EnvConst.AUTHORIZATION_HEADER))
            val authentication = extractAuthentication(token)
            SecurityContextHolder.getContext().authentication = authentication
            chain.doFilter(req, res)
            logger.info(mapOf<String, Any?>(
                    "method" to "doFilterInternal",
                    "return" to "void",
                    "message" to "Authentication with token: $token"
            ).toString())
        } catch (e: AccessDeniedException) {
            logger.error(mapOf<String, Any?>(
                    "method" to "doFilterInternal",
                    "return" to e.toString(),
                    "message" to "Access denied: ${e.message ?: "No message"}"
            ).toString())
            res.status = 401
            res.writer.write("Access denied")
        }
    }

    /**
     * Extract token from header
     */
    private fun extractToken(header: String?): String? {
        val headers = header?.split("Bearer ")

        return if (headers == null || headers.size < 2) {
            null
        } else {
            headers[1]
        }
    }

    /**
     * Extract authentication details from token
     */
    @Throws(AccessDeniedException::class)
    private fun extractAuthentication(token: String?): AuthenticationTokenDTO? {

        if (token == null) {
            logger.info(mapOf<String, Any?>(
                    "method" to "extractAuthentication",
                    "return" to "null",
                    "message" to "extractAuthentication return null"
            ).toString())
            return null
        }

        return try {
            val claims = processor.process(token, null)
            val retVal = AuthenticationTokenDTO(token, claims)
            logger.info(mapOf<String, Any?>(
                    "method" to "extractAuthentication",
                    "return" to retVal.toString(),
                    "message" to "extractAuthentication return null"
            ).toString())

            retVal
        } catch (e: Exception) {
            logger.info(mapOf<String, Any?>(
                    "method" to "extractAuthentication",
                    "return" to e.toString(),
                    "message" to "extractAuthentication error ${e.javaClass.simpleName} (${e.message ?: "No message"})"
            ).toString())
            throw AccessDeniedException("${e.javaClass.simpleName} (${e.message ?: "No message"})")
        }
    }
}

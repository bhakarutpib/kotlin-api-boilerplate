package com.ktaxa.helloproj

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HelloApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            System.getenv().forEach { (k, v) -> println("$k : $v") }
            runApplication<HelloApplication>(*args)
        }

    }
}

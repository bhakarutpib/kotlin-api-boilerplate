package com.ktaxa.helloproj.service

import com.ktaxa.helloproj.dto.StudentDTO
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate


@Service
class StudentService(private val restTemplate: RestTemplate) {

    fun findOne(id: String): StudentDTO? {
        // business logic should be here!!
        return if (id == "00000") {
            null
        } else {
            return StudentDTO(
                    name = "foo",
                    id = id,
                    passportNumber = "foobar"
            )
        }

    }

    // method below use resttemplate client to fetch any data
//    fun findOneThought3rdPartyAPI(id: String): ResponseEntity<StudentDTO>? {
//        val url = "https://3rdpartyendpoint.com"
//        return restTemplate.exchange(url, HttpMethod.GET, null, StudentDTO::class.java)
//    }
}

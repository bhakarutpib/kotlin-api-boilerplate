package com.ktaxa.helloproj.controller

import com.ktaxa.helloproj.dto.StudentDTO
import com.ktaxa.helloproj.service.StudentService
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@ExtendWith(SpringExtension::class)
@WebMvcTest(StudentController::class, secure = false)
class StudentControllerTest(@Autowired var mockMvc: MockMvc) {

    @MockBean
    private lateinit var studentService: StudentService

    @Test
    fun `should return correctly http status and body`() {
        whenever(studentService.findOne(anyString())).thenReturn(StudentDTO(
                name = "foo",
                passportNumber = "foobar",
                id = "1111"
        ))
        mockMvc.perform(MockMvcRequestBuilders.get("/students/1111"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("\$.name").value("foo"))
                .andExpect(jsonPath("\$.id").value("1111"))
                .andExpect(jsonPath("\$.passportNumber").value("foobar"))
    }

}
